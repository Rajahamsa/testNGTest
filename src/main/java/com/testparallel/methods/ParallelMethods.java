package com.testparallel.methods;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ParallelMethods {

	protected WebDriver driver;

	protected WebDriver getDriverInstance() {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\rjanagam\\Downloads\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		return driver;
	}

	@BeforeMethod
	public void setUp() {
		driver = getDriverInstance();

	}

	@FindBy(id = "nav-link-accountList")
	private WebElement signin;

	@Test
	public void navigatetoAmazonHomepage() {
		driver.get("https://www.amazon.com/");

	}

	@Test
	public void navigatetoGoogleHomepage() {
		driver.get("https://www.google.com/");

	}

	@Test
	public void navigatetoFacebookHomepage() {
		driver.get("https://www.facebook.com/");

	}

	@AfterMethod
	protected void close() {
		driver.close();
	}

}
