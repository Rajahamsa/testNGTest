package com.differentbrowsers.test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.ie.InternetExplorerDriver;

import org.testng.annotations.AfterClass;

import org.testng.annotations.BeforeClass;

import org.testng.annotations.Parameters;

import org.testng.annotations.Test;

public class MultiBrowser {

	public WebDriver driver;

	@Parameters("browser")

	@BeforeClass

	public void beforeTest(String browser) {

		if (browser.equalsIgnoreCase("chrome")) {

			System.setProperty("webdriver.chrome.driver",
					"C:\\Users\\rjanagam\\Downloads\\chromedriver_win32\\chromedriver.exe");

			driver = new ChromeDriver();

		} else if (browser.equalsIgnoreCase("ie")) {

			System.setProperty("webdriver.ie.driver",
					"C:\\Users\\rjanagam\\Downloads\\IEDriverServer_x64_3.6.0\\IEDriverServer.exe");

			driver = new InternetExplorerDriver();

		}

	}

	@Test
	public void test() {

		driver.get("http://www.google.com");
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

	}

	@Test
	public void testFacebook() {

		driver.get("http://www.facebook.com");
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

	}

	@AfterClass
	public void afterTest() {

		driver.quit();

	}

}